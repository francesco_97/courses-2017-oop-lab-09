package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

    private static final double NUM = -5;
    private String stringa;
    private double count, count3;
    private int count2;
    private Song canzone;
    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();
    /**
     * 
     * @return stringa
     */
    public String getStringa() {
        return stringa;
    }

    /**
     * 
     * 
     * @param stringa
     *          set stringa
     */
    public void setStringa(final String stringa) {
        this.stringa = stringa;
    }
    /**
     * 
     * @return canzone
     */
    public Song getCanzone() {
        return canzone;
    }

    /**
     * 
     * 
     * @param canzone
     *          set canzone
     */
    public void setCanzone(final Song canzone) {
        this.canzone = canzone;
    }

    /**
     * 
     * 
     * 
     * @return count
     */
    public double getCount() {
        return count;
    }

    /**
     * 
     * 
     * @param count
     *          set count
     */
    public void setCount3(final double count) {
        this.count3 = count;
    }
    /**
     * 
     * @return count 3
     */
    public double getCount3() {
        return count3;
    }

    /**
     * 
     * 
     * @param count
     *          set count
     */
    public void setCount(final double count) {
        this.count = count;
    }
    /**
     * 
     * 
     * @param count
     *          set count
     */
    public void setCount2(final int count) {
        this.count2 = count;
    }
    /**
     * 
     * 
     * 
     * @return count
     */
    public int getCount2() {
        return count2;
    }


    /**
     * 
     * @param albumName 
     *          nome album
     * @param year
     *          year album
     */
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    /**
     * @param songName 
     *          nome song
     * @param albumName
     *          name album
     * @param duration
     *          album duration
     * 
     * 
     */
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    /**
     * 
     * @return ordered list song
     */
    public Stream<String> orderedSongNames() {
        final List<String> stringa = new LinkedList<>();
        songs.stream().
        sorted((s1, s2)-> s1.getSongName().compareTo(s2.getSongName()))
        .forEach(e-> stringa.add(e.getSongName()));
        /*songs.stream().sorted(new Comparator<Song>() {
            public int compare(final Song o1, final Song o2) {
                return o1.getSongName().compareTo(o2.getSongName());
            }
        }).forEach(e-> stringa.add(e.getSongName()));*/
       return stringa.stream();
    }

    /**
     * 
     * @return album name
     */
    public Stream<String> albumNames() {
        final List<String> album = new LinkedList<>();
        songs.stream()
             .forEach(e-> album.add((e.getAlbumName().orElse(null) != null ? e.getAlbumName().get() : "no album")));
        return album.stream();
    }

    /**
     * 
     * @param year
     *          y
     * @return number of album
     */
    public Stream<String> albumInYear(final int year) {
        final List<String> number = new LinkedList<>();
        albums.forEach((a, y)-> {
            if (y.intValue() == year) {
                number.add(a);
            }
        });
        return number.stream();
    }

    /**
     * 
     * @param albumName
     *          name Album
     * 
     * @return count song album
     */
    public int countSongs(final String albumName) {
        this.setCount2(0);
        songs.stream()
        .forEach(s-> {
            final String name = (s.getAlbumName().orElse(null) != null ? s.getAlbumName().get() :  Optional.ofNullable(s.getAlbumName()).toString());
            if (name.equals(albumName)) {
                this.setCount2(this.getCount2() + 1);
            }
        });
        return this.getCount2();
    }

    /**
     * 
     * 
     * @return count song noalbum
     */
    public int countSongsInNoAlbum() {
        this.setCount2(0);
        songs.forEach(s-> {
                  if (!s.getAlbumName().isPresent()) {
                      this.setCount2(this.getCount2() + 1);
                  }
        });
        return this.getCount2();
    }

    /**
     * 
     * @param albumName
     *          name Album
     * 
     * @return optional double duration
     */
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        this.setCount(0);
        this.setCount2(0);
        songs.stream().
              forEach(s-> {
                  final String name = (s.getAlbumName().orElse(null) != null ? s.getAlbumName().get() :  Optional.ofNullable(s.getAlbumName()).toString());
                  if (name.equals(albumName)) {
                      this.setCount(s.getDuration() + this.getCount());
                      this.setCount2(this.getCount2() + 1);
                  }
        });
        return OptionalDouble.of(this.getCount() / this.getCount2());
    }


    /**
     * 
     * @return optional string nome canzone
     */
    public Optional<String> longestSong() {
        this.setCanzone(null);
        songs.stream().
        forEach(s-> {
            if (this.getCanzone() == null) {
                this.setCanzone(s); 
            } else {
                if (this.getCanzone().getDuration() < s.getDuration()) {
                    this.setCanzone(s);
                }
            }
        });
        return Optional.of(this.getCanzone().getSongName());
    }

    @Override
    public Optional<String> longestAlbum() {
        this.setCount(NUM);
        this.setCount3(NUM);
        this.setStringa(null);
        final Map<String, Double> m = new HashMap<>();
        songs.stream().
        forEach(s-> {
            final String name = (s.getAlbumName().orElse(null) != null ? s.getAlbumName().get() :  Optional.ofNullable(s.getAlbumName()).toString());
            if (m.containsKey(name)) {
                final double temp = m.get(name);
                m.remove(s.getAlbumName().get());
                m.put(name, temp + s.getDuration());
            } else {
                m.put(name, s.getDuration());
            }
            m.forEach((a, d)-> {
                if (this.getCount3() == NUM) {
                    this.setCount3(d);
                    this.setStringa(a);
                } else if (d > this.getCount3()) {
                    this.setCount3(d);
                    this.setStringa(a);
                }
            });
        });
        return Optional.of(this.getStringa());
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
