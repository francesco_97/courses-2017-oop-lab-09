package it.unibo.oop.lab.lambda.ex01;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This class will contain four utility functions on lists and maps, of which the first one is provided as exmaple.
 * 
 * All such methods take as second argument a functional interface from the Java library (java.util.function).
 * This enables calling them by using the concise lambda syntax, as it's done in the main function.
 * 
 * Realize the three methods **WITHOUT** using the Stream library, but only leveraging the lambdas.
 *
 */
public final class LambdaUtilities {


    private LambdaUtilities() {
    }

    /**
     * @param list
     *            the input list
     * @param op
     *            the process to run on each element
     * @param <T>
     *            element type
     * @return a new list containing, for each element of list, the element and
     *         a processed version
     */
    public static <T> List<T> dup(final List<T> list, final UnaryOperator<T> op) {
        final List<T> l = new ArrayList<>(list.size() * 2);
        list.forEach(t -> {
            l.add(t);
            l.add(op.apply(t));
        });
        return l;
    }

    /**
     * @param list
     *            input list
     * @param pre
     *            predicate to execute
     * @param <T>
     *            element type
     * @return a list where each value is an Optional, holding the previous
     *         value only if the predicate passes, and an Empty optional
     *         otherwise.
     */
    public static <T> List<Optional<T>> optFilter(final List<T> list, final Predicate<T> pre) {
        /*
         * Suggestion: consider Optional.filter
         */
        /* 
         * Dal main gli passo un predicate che mette solo i risultati divisibili
         * per 3, quindi dalla lista prima, vengono filtrati tramite 
         * il metodo filter(pre), dove pre è la funzione che filtra i risultati
         * detti prima. 
         * Gli elementi non divisibili per tre, verranno considerati come null,
         * quindi tramite la Optional.ofNullable(t), dove t è l'elemento 
         * considerato al momento, quelli filtrati correttamente vengono 
         * visualizzati con la scritta Optional[valore], quelli null con il 
         * valore Optional[empty]. 
         */
        final List<Optional<T>> l = new ArrayList<>();
        list.forEach(t-> l.add(Optional.ofNullable(t).filter(pre)));
        return l;
    }

    /**
     * @param list
     *            input list
     * @param op
     *            a function that, for each element, computes a key
     * @param <T>
     *            element type
     * @param <R>
     *            key type
     * @return a map that groups into categories each element of the input list,
     *         based on the mapping done by the function
     */
    public static <R, T> Map<R, Set<T>> group(final List<T> list, final Function<T, R> op) {
        /*
         * Suggestion: consider Map.merge
         */
        /*(s0,s1)-> {
                if(s0.addAll(s1)){
                }
                return s0;
            });*/
        final Map<R, Set<T>> m = new HashMap<>();
        list.forEach(t-> {
            final Set<T> set = new HashSet<>();
            set.add(t);
            m.merge(op.apply(t), set, (s0, s1)-> { 
                if (!s0.addAll(s1)) {
                    System.out.println("error\n");
                }
                return s0;
                });
            });
        /* In pratica per ogni elemento della lista aggiungo in un set temporaneo
         * l'elemento attuale della lista. Faccio una merge della mappa:
         * 1) il primo argomento che passo è la chiave in cui devo aggiungere
         *    l'elemento (o se non esiste la crea in automatico), che in questo 
         *    caso viene creata tramite la funzione op, che si rifà alla 
         *    alla funzione passata dal main.
         * 2) il valore (in questo caso un set) da usare se la chiave non è 
         *    presente, o da aggiungere se lo è.
         * 3) la funzione che aggiorna il valore basandosi sulla chiave, in
         *    cui gli elementi vecchi vengono aggiunti ai nuovi(il primo elemento
         *    della funzione è il set corrispondente alla chiave, il secondo il
         *    set contenente i nuovi valori da aggiungere.
         */
        return m;
    }

    /**
     * @param map
     *            input map
     * @param def
     *            the supplier
     * @param <V>
     *            element type
     * @param <K>
     *            key type
     * @return a map whose non present values are filled with the value provided
     *         by the supplier
     */
    public static <K, V> Map<K, V> fill(final Map<K, Optional<V>> map, final Supplier<V> def) {
        /*
         * Suggestion: consider Optional.orElse
         * 
         * Keep in mind that a map can be iterated through its forEach method
         */
        final Map<K, V> m = new HashMap<>();
        map.forEach((s1, s2)-> {
            m.put(s1, (s2.orElse(null) != null ? s2.get() : def.get()));
        });
        return m;
        /*
         * Uso il for each nella mappa passata da funzione. s1 è la chiave. s2 
         * è il valore corrispondente alla chiave (in questo caso un optional v).
         * Quindi s2 ha all'interno la possibilità di essere vuoto o con un 
         * valore.
         * nella nuova mappa metto la chiave s1 e i valori della mappa del 
         * main solo se presenti, altrimenti un valore random dato dalla funzione
         * def, che restituisce un numero random. 
         * Per vedere se il valore è vuoto, usi l'orElse(null) che ritorna null 
         * se null è presente (quindi se è vuoto) e other se non è null.
         * Se è other ritorno il valore di s2.get, sennò il valore di def.get()
         * (in questo caso sono tutti null tranne il terzo valore 
         * (che ha chiave 2), e il sesto valore(che ha chiave 5).
         */
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String[] args) {
        final List<Integer> li = IntStream.range(1, 8).mapToObj(i -> Integer.valueOf(i)).collect(Collectors.toList());
        System.out.println(dup(li, x -> x + 100));
        /*
         * [1, 101, 2, 102, 3, 103, 4, 104, 5, 105, 6, 106, 7, 107]
         */
        System.out.println(group(li, x -> x % 2 == 0 ? "even" : "odd"));
        /*
         * {odd=[1, 3, 5, 7], even=[2, 4, 6]}
         */
        final List<Optional<Integer>> opt = optFilter(li, x -> x % 3 == 0);
        System.out.println(opt);
        /*
         * [Optional.empty, Optional.empty, Optional[3], Optional.empty,
         * Optional.empty, Optional[6], Optional.empty]
         */
        final Map<Integer, Optional<Integer>> map = new HashMap<>();
        for (int i = 0; i < opt.size(); i++) {
            map.put(i, opt.get(i));
        }
        System.out.println(fill(map, () -> (int) (-Math.random() * 10)));
        /*
         * {0=-2, 1=-7, 2=3, 3=-3, 4=-7, 5=6, 6=-3}
         */
    }
}
